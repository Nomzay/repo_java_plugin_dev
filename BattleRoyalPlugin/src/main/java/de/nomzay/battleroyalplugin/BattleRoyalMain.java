package de.nomzay.battleroyalplugin;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.Bukkit;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.NamespacedKey;

import org.bukkit.enchantments.Enchantment;

import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import de.nomzay.battleroyalplugin.listener.DeathListener;
import de.nomzay.battleroyalplugin.listener.OtherListener;
import de.nomzay.battleroyalplugin.commands.BrCommand;

import java.util.Arrays;

public class BattleRoyalMain extends JavaPlugin {
    @Override
    public void onEnable(){
        getLogger().info("onEnable is called!");
        this.getCommand("br").setExecutor(new BrCommand());
        getServer().getPluginManager().registerEvents(new DeathListener(),this);
        getServer().getPluginManager().registerEvents(new OtherListener(this), this);
        setupHead();
    }

    @Override
    public void onDisable() {
        getLogger().info("onDisable is called");
    }

    private void setupHead(){
        ItemStack goldenHead = new ItemStack(Material.GOLDEN_APPLE);
        //dummy enchantment
        goldenHead.addUnsafeEnchantment(Enchantment.LUCK, 1);
        ItemMeta gMeta = goldenHead.getItemMeta();
        gMeta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
        gMeta.setDisplayName(ChatColor.AQUA + "Golden Head");
        gMeta.setLore(Arrays.asList("Dieser Golden Head verleiht dir ","einen stärkeren Effekt als ein Goldener Apfel!"));
        goldenHead.setItemMeta(gMeta);

        NamespacedKey key = new NamespacedKey(this,"goldenHead");
        ShapedRecipe recipeGoldenHead = new ShapedRecipe(key,goldenHead);
        recipeGoldenHead.shape("GGG","GHG","GGG");
        recipeGoldenHead.setIngredient('G',Material.GOLD_INGOT);
        recipeGoldenHead.setIngredient('H',Material.PLAYER_HEAD);

        Bukkit.addRecipe(recipeGoldenHead);


    }
}
