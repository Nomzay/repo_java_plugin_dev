package de.nomzay.battleroyalplugin.chatformater;

import net.md_5.bungee.api.ChatColor;

public class ChatCustomization {
    public static final String PREFIX = ChatColor.GRAY +">>" + ChatColor.RED + "Server" + ChatColor.GRAY + " | "
            + ChatColor.RESET;

    public static final String TITLE = ""+ChatColor.AQUA + ChatColor.MAGIC + " H " + ChatColor.RESET + ChatColor.GOLD
            + ChatColor.UNDERLINE + "Battle Royal" + ChatColor.RESET + ChatColor.AQUA + ChatColor.MAGIC + " H "
            + ChatColor.RESET +"\n";

    public static final String WELCOME = "" + ChatColor.AQUA + "Willkommen zu unserem Event "
            + ChatColor.DARK_RED + ChatColor.UNDERLINE;

    public static final String PROVIDEDBY = "" + ChatColor.GRAY + "Ein Event von " + ChatColor.DARK_GREEN
            + "growing-trees.de"+"\n";
}
