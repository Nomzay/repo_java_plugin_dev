package de.nomzay.battleroyalplugin.listener;

import org.bukkit.event.Listener;
import org.bukkit.Bukkit;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import de.nomzay.battleroyalplugin.*;
import de.nomzay.battleroyalplugin.chatformater.ChatCustomization;


public class OtherListener implements Listener {
    private final BattleRoyalMain plugin;

    public OtherListener(BattleRoyalMain plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onPlayerChangeWorld(PlayerChangedWorldEvent event){
        String netherJoinMessage = ChatCustomization.PREFIX + ChatColor.GREEN +"The player " + ChatColor.RESET
                + event.getPlayer().getDisplayName() + " joined the " + ChatColor.RED + "Nether";
        String netherLeaveMessage = ChatCustomization.PREFIX + ChatColor.YELLOW +"The player " + ChatColor.RESET
                + event.getPlayer().getDisplayName() + " leaved the " + ChatColor.RED + "Nether";

        if(event.getPlayer().getWorld().getName().contains("nether"))
            Bukkit.broadcastMessage(netherJoinMessage);
        else if(event.getFrom().getName().contains("nether"))
            Bukkit.broadcastMessage(netherLeaveMessage);

    }

    @EventHandler
    public void onConsume(PlayerItemConsumeEvent event){
        if(!(event.getItem().getItemMeta().getDisplayName().contains("Golden Head")))
            return;
        Player player = event.getPlayer();
        String effectsMessage = ChatCustomization.PREFIX + ChatColor.GRAY + "The effects from "
                + event.getItem().getItemMeta().getDisplayName() + ChatColor.GRAY + " were applied";
        //Adds Regeneration II, Speed I and Strength I to Absorption (from Golden Apple)
        player.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION,300,1));
        player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED,1200,0));
        player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE,1200,0));
        player.sendMessage(effectsMessage);

    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event){

        final PlayerJoinEvent finalEvent = event;
        final String welcomeMessage = ""
                +"\n"
                +"\n"
                +"\n"
                +"\n"
                + "                         " + ChatCustomization.TITLE
                +"\n"
                +"\n"
                + "                 " + ChatCustomization.WELCOME + event.getPlayer().getDisplayName()
                + ChatColor.RESET
                +"\n"
                + "                 " + ChatCustomization.PROVIDEDBY
                +"\n"
                +"\n"
                +"";

        final String[] str = welcomeMessage.split("\n");
        final String joinMessage = ChatCustomization.PREFIX + event.getPlayer().getDisplayName()
                + ChatColor.GREEN
                + " connected to the Server";
        finalEvent.setJoinMessage(joinMessage);
        new BukkitRunnable(){
            @Override
            public void run() {
                finalEvent.getPlayer().sendMessage(str);
            }
        }.runTaskLaterAsynchronously(this.plugin, 20);

    }

    @EventHandler
    public void onPlayerDisconnect(PlayerQuitEvent event){
        final String disconnectMessage = ChatCustomization.PREFIX + event.getPlayer().getDisplayName()
                + " disconnected from the Server";
        event.setQuitMessage(disconnectMessage);

    }
}
