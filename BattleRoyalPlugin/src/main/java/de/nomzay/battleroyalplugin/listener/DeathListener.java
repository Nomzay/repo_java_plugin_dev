package de.nomzay.battleroyalplugin.listener;


import de.nomzay.battleroyalplugin.chatformater.ChatCustomization;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import net.md_5.bungee.api.ChatColor;

public class DeathListener implements Listener {
    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event){
        String deathMessage = ChatCustomization.PREFIX + ChatColor.DARK_PURPLE + event.getEntity().getDisplayName()
                + ChatColor.RED + " died!";
        String eliminatedByOtherMessage = ChatCustomization.PREFIX + ChatColor.GRAY+"You are eliminated";


        if(!(event.getEntity().getKiller() instanceof Player)){
            event.setDeathMessage(deathMessage);
            Player player = event.getEntity();
            player.sendMessage(eliminatedByOtherMessage);
            player.setGameMode(GameMode.SPECTATOR);
            return;
        }
        String eliminatedByPlayerMessage = ChatCustomization.PREFIX + ChatColor.RED + "The Player"
                + ChatColor.DARK_PURPLE + event.getEntity().getDisplayName() +ChatColor.RED
                + " was killed by " + event.getEntity().getKiller().getDisplayName() +ChatColor.RED+  "!";

        event.setDeathMessage(eliminatedByPlayerMessage);

        Player player = (Player) event.getEntity();
        giveGoldenHead(player);

        player.sendMessage(eliminatedByOtherMessage);
        player.setGameMode(GameMode.SPECTATOR);



    }

    private void giveGoldenHead(Player player){
        ItemStack playerSkull = new ItemStack(Material.PLAYER_HEAD,1);
        SkullMeta pMeta = (SkullMeta)  playerSkull.getItemMeta();
        pMeta.setOwningPlayer(player.getPlayer());
        pMeta.setDisplayName(ChatColor.AQUA +player.getName() + "'s Head");
        playerSkull.setItemMeta(pMeta);
        player.getKiller().getInventory().addItem(playerSkull);
    }

}
