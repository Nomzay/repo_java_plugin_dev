package de.nomzay.battleroyalplugin.brgame;


import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Game {
    private ArrayList<Player> playerArrayList = new ArrayList<Player>();
    private boolean sessionBool;

    public boolean isSessionBool() {
        return sessionBool;
    }

    public void setSessionBool(boolean sessionBool) {
        this.sessionBool = sessionBool;
    }

    public void setPlayerFromArray(Player player){
        if(playerArrayList.contains(player))
            return;
        playerArrayList.add(player);
    }

    public void removePlayerFromArray(Player player){
        if(!(playerArrayList.contains(player)))
            return;
        playerArrayList.remove(player);
    }


}
