package de.nomzay.battleroyalplugin.commands;

import de.nomzay.battleroyalplugin.brgame.Game;
import de.nomzay.battleroyalplugin.chatformater.ChatCustomization;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;



public class BrCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            commandSender.sendMessage("You need to be a player.");
            return true;
        }
        Player player = (Player) commandSender;

        if(!(player.isOp())){
            player.sendMessage(ChatColor.RED + "You need to be an operator to issue this command.");
            return true;
        }

        if(strings.length == 0){
            player.sendMessage(ChatColor.RED
                    + "How to use /br: create [session name], add [player], remove [player], clear [session name], border [size]");
            return true;
        }
        Game gameSession = new Game();
        if(strings[0].equalsIgnoreCase("border") && strings.length == 2){
            player.sendMessage(ChatCustomization.PREFIX + ChatColor.GRAY
                    + "Your location will set as center.");

            boolean setCheck = setBrBorder(strings[1],player.getLocation());
            if(!setCheck){
                player.sendMessage(ChatCustomization.PREFIX + ChatColor.RED
                        + "Please type a size behind the command");
                return true;
            }
            player.sendMessage(ChatCustomization.PREFIX + ChatColor.GREEN
                        + "Your border has been setted.");
            return true;
        }else if(strings[0].equalsIgnoreCase("start") && strings.length == 1){

            if(gameSession.isSessionBool()){
                player.sendMessage(ChatCustomization.PREFIX + ChatColor.GRAY
                        + "The game session is already started");
                return true;
            }
            gameSession.setSessionBool(true);
            return true;

        }else if(strings[0].equalsIgnoreCase("add") && strings.length == 2){
            
        }


        return true;
    }

    public boolean setBrBorder(String str, Location location){
        World world = Bukkit.getWorld("world");
        if(world == null){
            return false;
        }
        world.getWorldBorder().setCenter(location);
        double size = 0;

        try{
            size = Double.parseDouble(str);
        }catch(NumberFormatException e){
            e.printStackTrace();
        }

        if(size == 0){
            return false;
        }

        world.getWorldBorder().setSize(size);
        return true;
    }
}
